import React, { useState } from 'react';
import  Button  from './components/Button.jsx'
import Modal from './components/Modal/Modal.jsx';
import ModalWrapper from './components/Modal/ModalWrapper.jsx';
import ModalHeader from './components/Modal/ModalHeader.jsx';
import ModalFooter from './components/Modal/ModalFooter.jsx';
import ModalClose from './components/Modal/ModalClose.jsx';
import ModalBody from './components/Modal/ModalBody.jsx';
import ModalImage from './components/Modal/ModalImage.jsx';
import ModalText from './components/Modal/ModalText.jsx';
import './style.scss';



  function App() {
    const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
    const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);
  
    const openFirstModal = () => setIsFirstModalOpen(true);
    const closeFirstModal = () => setIsFirstModalOpen(false);
  
    const openSecondModal = () => setIsSecondModalOpen(true);
    const closeSecondModal = () => setIsSecondModalOpen(false);

  return (
    <div>
    <Button onClick={openFirstModal}>Open first modal</Button>
    <Button onClick={openSecondModal}>Open second modal</Button>

    {isFirstModalOpen && (
        <Modal onClose={closeFirstModal}>
          <ModalImage onClose={closeFirstModal} />
        </Modal>
      )}

      {isSecondModalOpen && (
        <Modal onClose={closeSecondModal}>
          <ModalText onClose={closeSecondModal} />
        </Modal>
      )}

  </div>
   
  )
}


export default App