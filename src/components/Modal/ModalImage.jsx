import React from 'react';

import ModalHeader from './ModalHeader';
import ModalFooter from './ModalFooter';
import ModalClose from './ModalClose';
import ModalBody from './ModalBody';

const ModalImage = ({ onClose }) => {
  return (
    <div className='modal-img'>
       
      <ModalHeader>
        {<h1>Delete of product?</h1>}
        <ModalClose onClick={onClose} />
      </ModalHeader>
      <ModalBody>
        <p>Product Delete!</p>
      </ModalBody>
      <ModalFooter
        firstText="CANCEL"
        firstClick={onClose}
        secondaryText="DELETE"
        secondaryClick={() => console.log('Підтверджено')}
      />
    </div>
  );
};

export default ModalImage;