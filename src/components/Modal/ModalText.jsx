import React from 'react';

import ModalHeader from './ModalHeader';
import ModalFooter from './ModalFooter';
import ModalClose from './ModalClose';
import ModalBody from './ModalBody';

const ModalText = ({ onClose }) => {
  return (
    
 <div className='modal-text '>
      <ModalHeader>
        <h2>Add Product "NAME"</h2>
        <ModalClose onClick={onClose} />
      </ModalHeader>
      <ModalBody>
        <p>Description for you product</p>
      </ModalBody>
      <ModalFooter
        
        firstClick={onClose}
        secondaryText="ADD TO FAVORITE"
        secondaryClick={() => console.log('Підтверджено')}
      />
    </div>

   
  );
};

export default ModalText;