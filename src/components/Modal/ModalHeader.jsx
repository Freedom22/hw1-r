

import React from 'react';

function ModalHeader ({children}){
    return(
        <div className="modal-header">
            {children}
        </div>
    )
}

export default ModalHeader;
