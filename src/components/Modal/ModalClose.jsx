import React from 'react';

function ModalClose({onClick}){

    return(
        <button className="modal-close" onClick={onClick}>&times;</button>
    )
}

export default ModalClose