import React from "react";
import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalFooter from "./ModalFooter";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";

function Modal ({children}){
    return(
        <div className="modal">
            {children}
        </div>
    )
}

export default Modal