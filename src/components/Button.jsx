import React from 'react';


 function Button({ type = 'button', classNames = '', onClick, children }){
    return(
          
        <div>
            <button  type={type} className={classNames} onClick={onClick}> {children}</button>
            
        </div>
          
    )
}

// Button.propTypes = {
//     type: PropTypes.string,
//     classNames: PropTypes.string,
//     onClick: PropTypes.func.isRequired,
//     children: PropTypes.node.isRequired,
// }

export default Button 
